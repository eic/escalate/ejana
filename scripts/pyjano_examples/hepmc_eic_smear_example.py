from pyjano.jana import Jana

jana = Jana(nevents=10000, output='open_charm_18x275_10k.root') \
    .plugin('hepmc_reader') \
    .plugin('eic_smear', detector='jleic') \
    .plugin('event_writer') \
    .plugin('open_charm') \
    .source('/home/romanov/Downloads/herwig6_P19104_Q1_N1e+06_e-p_18x275.hepmc')

jana.run(retval_raise=False)

