#ifndef EJANA_DISINFO_H
#define EJANA_DISINFO_H

#include <JANA/JObject.h>

namespace minimodel {
    struct DisInfo: public JObject {
        double q2;
        double x;
        double y;
        double w2;
        double nu;
        double t_hat;
    };
}
#endif //EJANA_DISINFO_H
