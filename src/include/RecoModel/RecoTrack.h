// $Id$
//
//    File: VertexTrackerTrack.h
// Created: Thu Jan  5 21:58:34 EST 2017
// Creator: davidl (on Darwin harriet 15.6.0 i386)
//

#ifndef _VertexTrackerTrack_
#define _VertexTrackerTrack_

#include <JANA/JObject.h>
#include <JANA/JFactoryT.h>
#include <TVector3.h>

class RecoTrack: public JObject{
	public:
		JOBJECT_PUBLIC(RecoTrack);
	    TVector3 x;
		TVector3 p;
		double theta;
		double charge;
		double phi;
		int    q;
		double chisq;
		double ndf;
		uint64_t track_id;  // For use with MC

		RecoTrack() = default;
		RecoTrack(const RecoTrack& other) = default;
};


/// Metadata that all JFactoryT<RecoTrack> should report
template <> struct JMetadata<RecoTrack> {
    uint64_t total_track_count = 0;
    uint64_t succeeded_count = 0;
    uint64_t insufficient_data_count = 0;
    uint64_t exception_count = 0;
    uint64_t misfit_count = 0;
};


#endif // _VertexTrackerTrack_

