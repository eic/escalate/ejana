//
// Created by Nathan Brei on 3/31/20.
//

#ifndef EJANA_RECOVERTEX_H
#define EJANA_RECOVERTEX_H

#include <JANA/JObject.h>
#include <JANA/JFactory.h>
#include <TVector3.h>
#include "RecoTrack.h"

class RecoVertex: public JObject{
public:
    JOBJECT_PUBLIC(RecoTrack);
    TVector3 pos;
    std::vector<const RecoTrack*> tracks; // TODO: Make associated object?
};


/// Metadata that all JFactoryT<RecoVertex> should report
template <> struct JMetadata<RecoVertex> {
    uint64_t succeeded_count = 0;
    uint64_t insufficient_data_count = 0;
    uint64_t misfit_count = 0;
};


#endif //EJANA_RECOVERTEX_H
