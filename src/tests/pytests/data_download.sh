#!/bin/bash

[ ! -f "beagle_eD.txt" ] &&\
  wget "https://gitlab.com/eic/escalate/workspace/raw/master/data/beagle_eD.txt"

[ ! -f "eic-py6_ep_Q2-10-1000.18x275.100.txt" ] &&\
  wget "https://gitlab.com/eic/validation_data/raw/master/tiny_sets/eic-py6_ep_Q2-10-1000.18x275.100.txt"

[ ! -f "py8-dire_10k_no-crosa.hepmc" ] &&\
  wget "https://gitlab.com/eic/validation_data/raw/master/small_sets/py8-dire_10k_no-crosa.hepmc"

[ ! -f "herwig6_e-p_5x100_10k.hepmc" ] &&\
  wget "https://gitlab.com/eic/escalate/workspace/raw/master/data/herwig6_e-p_5x100_10k.hepmc"