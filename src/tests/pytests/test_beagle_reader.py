import unittest
import os

class TestBeagleReader(unittest.TestCase):

    def setUp(self) -> None:
        self.file_name = 'beagle_eD.txt'

    def test_vmeson_beagle(self):
        self.assertEqual('foo'.upper(), 'FOO')
        from pyjano.jana import Jana
        Jana()\
            .plugin('beagle_reader') \
            .plugin('vmeson') \
            .plugin('jana', nevents=10000, output='beagle.root') \
            .source('./beagle_eD.txt')\
            .run(retval_raise=True)

if __name__ == '__main__':
    unittest.main()
