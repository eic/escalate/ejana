import unittest
from pyjano.jana import Jana
import os

class TestHepmcReader(unittest.TestCase):

    def test_vmeson_hepmc(self):
        Jana(nevents=1000, output='eic_hepmc_reader.root')\
            .plugin('hepmc_reader') \
            .plugin('vmeson') \
            .plugin('event_writer')\
            .source('./py8-dire_10k_no-crosa.hepmc')\
            .run(retval_raise=True)


if __name__ == '__main__':
    unittest.main()
