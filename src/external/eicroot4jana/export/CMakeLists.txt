
include_directories(
${PROJECT_SOURCE_DIR}/fairroot
${PROJECT_SOURCE_DIR}/pandaroot
${PROJECT_SOURCE_DIR}/eicroot
${PROJECT_SOURCE_DIR}/import
${PROJECT_SOURCE_DIR}/hits
${PROJECT_SOURCE_DIR}/export

# eic-smear headers;
${CMAKE_INSTALL_PREFIX}/include
)

SET(EXPORT_LIB export)
# FIXME: unify this line and INSTALL(FILES) below in a single cmake module call;		
SET(EXPORT_ROOTMAP ${CMAKE_CURRENT_BINARY_DIR}/libexport_rdict.pcm ${CMAKE_CURRENT_BINARY_DIR}/libexport.rootmap)

# --------------------------------------------------------

SET( EXPORT_SRC
EicRcParticle.cxx
EicRcEvent.cxx
#EicEventAssembler.cxx

G__export.cxx
)

# In one line, no stupid extra SET() calls; FIXME: use eicroot.cmake;
ROOT_GENERATE_DICTIONARY(G__export EicRcParticle.h EicRcEvent.h LINKDEF exportLinkDef.h)

ADD_LIBRARY( ${EXPORT_LIB} SHARED ${EXPORT_SRC} )
TARGET_LINK_LIBRARIES(${EXPORT_LIB} ${ROOT_LIBRARIES} Geom fairroot eicroot import hits)

INSTALL(TARGETS ${EXPORT_LIB}     DESTINATION lib)
INSTALL(FILES   ${EXPORT_ROOTMAP} DESTINATION lib)
