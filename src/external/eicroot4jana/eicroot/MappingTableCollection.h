
#include <vector>
#include <map>

#include <TObject.h>
#include <TString.h>

#ifndef _MAPPING_TABLE_ 
#define _MAPPING_TABLE_ 

#define _DREAM_FEU_CHANNEL_COUNT_ 512

class MappingPlane: public TObject {
 public:
 MappingPlane(): mPitch(0.0) {};
  ~MappingPlane() { mDreamChannels.clear(); };

  void SetPitch(double pitch) { mPitch = pitch; };
  void AddStrip(unsigned channel) { mDreamChannels.push_back(channel); };

  double GetPitch( void ) const { return mPitch; };
  unsigned GetStripCount( void ) const { return mDreamChannels.size(); };
  int GetDreamChannel(unsigned strip) const { 
    return (strip < mDreamChannels.size() ? mDreamChannels[strip] : -1);
  };

 private:
  // Technically can link to column properties in case of Zigzags (and be 
  // able to pull out other properties); does not seem to be reasonable though;
  double mPitch;                         // pitch in [mm]

  // DREAM channels in the order "aligned" with either X- or Y-coordinate in 
  // the gerber file coord.system; NB: channels are not guaranteed to be 
  // contiguous in case of the diamond boards -> use std::vector;
  std::vector<unsigned> mDreamChannels;  // the actual strip->DREAM mapping

  ClassDef(MappingPlane,1)
};

class MappingSpot: public TObject {
 public:
 MappingSpot(): mDirect(0) {};
  ~MappingSpot() { 
    // FIXME: take care about mMappingPlanes[] as well;
    if (mDirect) delete [] mDirect; 
  };

  void AddMappingPlane(MappingPlane *plane) { mMappingPlanes.push_back(plane); };

  // Entries are saved in "reversed" order (planes book DREAM channel ranges); 
  // upon startup need to create a direct access table by looping through all 
  // the booked channels; assume of course that any given board is served by 
  // a single DREAM FEU;
  void CreateLookupTable( void );

  unsigned GetPlaneCount( void ) const { return mMappingPlanes.size(); };
  const MappingPlane *GetMappingPlane(unsigned pl) const { 
    return (pl < mMappingPlanes.size() ? mMappingPlanes[pl] : 0);
  };

  std::pair<int,int> GetPlaneAndStrip(unsigned dream);

 private:
  std::vector<MappingPlane*> mMappingPlanes; // the database of the member planes

  // DREAM->{plane,strip} direct access table; admittedly inefficient (an array 
  // with 512 entries will be created per spot -> perhaps ~2MB waste of RAM 
  // assuming 10 different board types); since reference channels will actually
  // be shared between neighboring spots see no good reason to create a 
  // per-board mapping table though;
  unsigned *mDirect;                         //!

  ClassDef(MappingSpot,1)
};

class MappingTable: public TObject {
 public:
 MappingTable(const char *tag = 0): mTag(tag ? tag : "") {};
  ~MappingTable() {};

  void AddMappingSpot(const TString &spot, MappingSpot *mspot) { mSpots[spot] = mspot; };

  const TString &GetTag( void )                      const { return mTag; };

  std::map<TString, MappingSpot*> &GetMappingSpots( void ) { return mSpots; };

  // NB: prefer NOT to create "pairs of spots" for diamond locations like "BC4" 
  // (so require exact match and even if a particular board had both "B4" and "C4" spots, 
  // still return 0 pointer); just want to avoid dealing with cases with a mixture of 
  // two different pitches; also centering at the reference strip locations rather
  // than at the cell center does not look about intuitive enough;
  MappingSpot *GetMappingSpot(const char *spot) {
    return (mSpots.find(spot) == mSpots.end() ? 0 : mSpots[spot]);
  };

 protected:
  TString mTag;                           // PCB tag

 private:
  // NB: in case of Zigzags there will be some degeneracy since the spots belonging 
  // to the same column will all have the same mapping to DREAM and the same pitch;
  // it is easier to create (up to five) independent entries though, and have 
  // interface completely symmetric between Diamonds and Zigzags; 
  std::map<TString, MappingSpot*> mSpots; // the database of PCB spots

  ClassDef(MappingTable,1)
};

class MappingTableCollection: public TObject {
 public:
  MappingTableCollection() {};
  ~MappingTableCollection() {};

  void AddMappingTable(MappingTable *mtable) { 
    mMappingTables[mtable->GetTag()] = mtable; 
  };
  MappingTable *GetMappingTable(const char *tag) {
    return (mMappingTables.find(tag) == mMappingTables.end() ? 0 : mMappingTables[tag]);
  };

  void CreateLookupTables( void );

 private:
  // NB: here and in several places above: since lookup table needs to be created
  // for every spot after import, 'const' modifiers can not be used;
  std::map<TString, MappingTable*> mMappingTables; // one per PCB

  ClassDef(MappingTableCollection,1)
};

#endif
