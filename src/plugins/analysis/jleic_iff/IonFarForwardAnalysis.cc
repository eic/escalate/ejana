#include <iostream>
#include <algorithm>

#include <TDirectory.h>
#include <TCanvas.h>
#include <TPad.h>

#include <JANA/JApplication.h>
#include <JANA/JEvent.h>

#include <fmt/core.h>

#include <ejana/EStringHelpers.h>
#include <ejana/EServicePool.h>

#include <MinimalistModel/McTrack.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <MinimalistModel/McFluxHit.h>

#include "IonFarForwardAnalysis.h"


using namespace fmt;

//------------------
// IonFarForwardAnalysis (Constructor)
//------------------
IonFarForwardAnalysis::IonFarForwardAnalysis(JApplication *app) :
	JEventProcessor(app),
	services(app)
{
}

//------------------
// Init
//------------------
void IonFarForwardAnalysis::Init()
{
	// Ask service locator a file to write to
	auto file = services.Get<TFile>();

	// Root related, we switch gDirectory to this file
	file->cd();
	gDirectory->pwd();

	// Create a directory for this plugin. And subdirectories for series of histograms
	dir_main = file->mkdir("sample_ana");

	hist_pt = new TH1F("pt", "particle pt", 100, 0, 10);
    hist_pt->SetDirectory(dir_main);

    hist_pz = new TH1F("pz", "particle pz", 100, 0, 200);
    hist_pz->SetDirectory(dir_main);
}


//------------------
// Process
//------------------
void IonFarForwardAnalysis::Process(const std::shared_ptr<const JEvent>& event)
{
    // >oO debug (you will be quickly annoyed by printing this line each event)
    // fmt::print("IonFarForwardAnalysis::Process() called. Event number: {}\n", event->GetEventNumber());

	auto particles = event->Get<minimodel::McGeneratedParticle>("");

 	for(auto partice: particles) {
 	    if(partice->pdg == 2212) {
 	        double pt = sqrt(partice->px*partice->px + partice->py*partice->py);
 	        hist_pt->Fill(pt);
 	    }
	}
}


//------------------
// Finish
//------------------
void IonFarForwardAnalysis::Finish()
{
	fmt::print("IonFarForwardAnalysis::Finish() called\n");
}


