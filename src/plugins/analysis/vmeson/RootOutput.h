#include <TFile.h>
#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TApplication.h>

class RootOutput 
{
public:
    void init(TFile *file)
    {
        // 'locker' locks mutex so other threads can't interfere with TFile doing its job
        std::lock_guard<std::recursive_mutex> locker(lock);

        // create a subdirectory "hist_dir" in this file
        dir_main = file->mkdir("vmeson");
        file->cd();         // Just in case some other root file is the main TDirectory now

        // TTree with recoiled electron
        tree_rec_e = new TTree("rec_e", "a Tree with vect");
        tree_rec_e->SetDirectory(dir_main);

        //------------------------------------ High-x  -----------------------------------------------------
        const int XBINS_h=19, Q2BINS_h=16;
        double xEdges_h[XBINS_h + 1] =
                {-3.8, -3.6, -3.4, -3.2, -3.,
                 -2.8, -2.6, -2.4, -2.2, -2.,
                 -1.8, -1.6, -1.4, -1.2, -1.,
                 -0.8, -0.6, -0.4, -0.2, 0.};


        Double_t Q2Edges_h[Q2BINS_h + 1] =
                {0.,   0.25,  0.5,  0.75, 1.,
                 1.25, 1.5,   1.75, 2.,   2.25,
                 2.5,  2.75,  3.,   3.25, 3.5,
                 3.75, 4.};

        h2_XQ2_true_log = new TH2I("h2_XQ2_true_log","True X,Q2 ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_true_log->SetDirectory(dir_main);
        h2_XQ2_true_log->GetXaxis()->SetTitle("log(x)"); h2_XQ2_true_log->GetYaxis()->SetTitle("log(Q^{2})");

        h2_XQ2_true = new TH2I("h2_XQ2_true","True X,Q2 ", 100, 0., 1., 100, 0.,5000.);
        h2_XQ2_true->SetDirectory(dir_main);
        h2_XQ2_true->GetXaxis()->SetTitle("x_{true}"); h2_XQ2_true->GetYaxis()->SetTitle("Q^{2}_{true}");

        hQ2_true = new TH1D("hQ2_true","True Q2 ",100, 0.,5000.);
        hQ2_true->SetDirectory(dir_main); hQ2_true->GetXaxis()->SetTitle("Q^{2}_{true} (GeV^2)");

        hX_true = new TH1D("hX_true","True X ",100, 0.,1.);
        hX_true->SetDirectory(dir_main); hX_true->GetXaxis()->SetTitle("x_{true}");

        hY_true = new TH1D("hY_true","True Y ",100, 0.,1.);
        hY_true->SetDirectory(dir_main); hY_true->GetXaxis()->SetTitle("y_{true}");

        hNu_true = new TH1D("hNu_true","True Nu ",100, 0.,2000.);
        hNu_true->SetDirectory(dir_main); hNu_true->GetXaxis()->SetTitle("Nu_{true}");

        hW2_true = new TH1D("hW2_true","True W2 ",100, 0.,2000.);
        hW2_true->SetDirectory(dir_main); hW2_true->GetXaxis()->SetTitle("W2_{true}");

        // -------- end true information ----------------------------------

        hXQ2_smear = new TH2I("hXQ2_smear","Smear X, Q2 ", XBINS_h, xEdges_h, Q2BINS_h, Q2Edges_h);
        hXQ2_smear->SetDirectory(dir_main);
        hXQ2_smear->GetXaxis()->SetTitle("log(x)");
        hXQ2_smear->GetYaxis()->SetTitle("log(Q^{2})");

        hpt_eth = new TH2D("Pt_vs_Pz_div_Ptot", "Pt vs Pz/Ptot", 100,1,100, 100,1,100);
        hpt_eth->SetDirectory(dir_main);

        e_theta = new TH1D("e_theta", "#theta_{e} ", 100,0.,180.);
        e_theta->SetDirectory(dir_main);
        e_eta = new TH1D("e_eta", "#eta_{e} ", 100,-3.5,3.5);
        e_eta->SetDirectory(dir_main);


        h_W2_jpsi = new TH1D("h_W2_jpsi","h_W2_jpsi ",100, 0.,2000.);
        h_W2_jpsi->SetDirectory(dir_main); h_W2_jpsi->GetXaxis()->SetTitle("W^2_{J/#psi}");


        h_t_true = new TH1D("h_t_true","h_t_true ",100, 0.,2.);
        h_t_true->SetDirectory(dir_main);
        h_t_true->GetXaxis()->SetTitle("t");

        h_myt = new TH1D("h_myt","h_t_jpsi ",100, 0.,2.);
        h_myt->SetDirectory(dir_main);
        h_myt->GetXaxis()->SetTitle("t");

        h_t_jpsi = new TH1D("h_t_jpsi","h_t_jpsi ",100, 0.,2.);
        h_t_jpsi->SetDirectory(dir_main);
        h_t_jpsi->GetXaxis()->SetTitle("t");

        h_myt_d = new TH1D("h_t_d","h_t_d ",100, 0.,2.);
        h_myt_d->SetDirectory(dir_main);
        h_myt_d->GetXaxis()->SetTitle("t");

        h_myt_n = new TH1D("h_t_n","h_t_n ",100, 0.,2.);
        h_myt_n->SetDirectory(dir_main);
        h_myt_n->GetXaxis()->SetTitle("t");

        h_mytheta = new TH1D("h_mytheta","h_mytheta",1000, -0.,30.);
        h_mytheta->SetDirectory(dir_main); h_mytheta->GetXaxis()->SetTitle("theta");

        h_theta_n = new TH1D("h_theta_n","#theta_{n}",1000, -0.,30.);
        h_theta_n->SetDirectory(dir_main); h_theta_n->GetXaxis()->SetTitle("#theta_{n}");

        h_theta_p = new TH1D("h_theta_p","#theta_{p}",1000, -0.,30.);
        h_theta_p->SetDirectory(dir_main); h_theta_p->GetXaxis()->SetTitle("#theta_{p}");

        h_theta_d = new TH1D("h_theta_d","#theta_{d}",1000, -0.,30.);
        h_theta_d->SetDirectory(dir_main); h_theta_d->GetXaxis()->SetTitle("#theta_{p}");

        h2_theta_pn = new TH2I("h2_mytheta_pn"," #theta_{p} vs #theta_{n}",100, 0.00,30.,100, 0., 30.);
        h2_theta_pn->SetDirectory(dir_main); h2_theta_pn->GetXaxis()->SetTitle("#theta_{p}(mrad)");
        h2_theta_pn->GetYaxis()->SetTitle(" #theta_{n}(mrad)");

        h2_mytheta_myt = new TH2I("h2_mytheta_myt"," theta vs t",100, 0.00,30.,100, 0., 2.);
        h2_mytheta_myt->SetDirectory(dir_main); h2_mytheta_myt->GetXaxis()->SetTitle("#theta (mrad)");
        h2_mytheta_myt->GetYaxis()->SetTitle("t");

        h2_theta_t_n = new TH2I("h2_theta_t_n"," #theta_{n} vs t_{n}",100, 0.00,30.,100, 0., 2.);
        h2_theta_t_n->SetDirectory(dir_main); h2_theta_t_n->GetXaxis()->SetTitle("#theta (mrad)");
        h2_theta_t_n->GetYaxis()->SetTitle("t");
        h2_theta_t_d = new TH2I("h2_theta_t_d"," #theta_{d} vs t_{d}",100, 0.00,30.,100, 0., 2.);
        h2_theta_t_d->SetDirectory(dir_main); h2_theta_t_d->GetXaxis()->SetTitle("#theta (mrad)");
        h2_theta_t_d->GetYaxis()->SetTitle("t");

        h2_theta_t_jpsi = new TH2I("h2_theta_t_jpsi"," #theta vs t_{jpsi}",100, 0.00,30.,100, 0., 2.);
        h2_theta_t_jpsi->SetDirectory(dir_main); h2_theta_t_n->GetXaxis()->SetTitle("#theta (mrad)");
        h2_theta_t_jpsi->GetYaxis()->SetTitle("t_{#jpsi}");

        h2_mytheta_t_true = new TH2I("h2_mytheta_t_true"," #theta vs t_{true}",100, 0.00,30.,100, 0., 2.);
        h2_mytheta_t_true->SetDirectory(dir_main); h2_mytheta_t_true->GetXaxis()->SetTitle("#theta (mrad)");
        h2_mytheta_t_true->GetYaxis()->SetTitle("t_{true}");

        h_Theta_jpsi= new TH1D("h_Theta_jpsi"," cos theta jpsi ",100, -1.,1.);
        h_Theta_jpsi->SetDirectory(dir_main); h_Theta_jpsi->GetXaxis()->SetTitle("cos(/#theta)_{J/#psi}");

        h_pt_jpsi = new TH1D("h_pt_jpsi","h_pt_jpsi ",100, 0.,10.);
        h_pt_jpsi->SetDirectory(dir_main); h_pt_jpsi->GetXaxis()->SetTitle("p_{T}^{J/#psi}");
        h_pt_jpsi10 = new TH1D("h_pt_jpsi10","h_pt_jpsi 10mrad",100, 0.,10.);
        h_pt_jpsi10->SetDirectory(dir_main); h_pt_jpsi10->GetXaxis()->SetTitle("p_{T}^{J/#psi}");
        h_pt_jpsi5 = new TH1D("h_pt_jpsi5","h_pt_jpsi 5mrad",100, 0.,10.);
        h_pt_jpsi5->SetDirectory(dir_main); h_pt_jpsi5->GetXaxis()->SetTitle("p_{T}^{J/#psi}");
        h_pt_jpsi2 = new TH1D("h_pt_jpsi2","h_pt_jpsi 2mrad",100, 0.,10.);
        h_pt_jpsi2->SetDirectory(dir_main); h_pt_jpsi2->GetXaxis()->SetTitle("p_{T}^{J/#psi}");

        h_t_jpsi10 = new TH1D("h_t_jpsi10","h_t_jpsi 10mrad",100, 0.,2.);
        h_t_jpsi10->SetDirectory(dir_main); h_t_jpsi10->GetXaxis()->SetTitle("t");
        h_t_jpsi5 = new TH1D("h_t_jpsi5","h_t_jpsi 5mrad",100, 0.,2.);
        h_t_jpsi5->SetDirectory(dir_main); h_t_jpsi5->GetXaxis()->SetTitle("t");
        h_t_jpsi2 = new TH1D("h_t_jpsi2","h_t_jpsi 2mrad",100, 0.,2.);
        h_t_jpsi2->SetDirectory(dir_main); h_t_jpsi2->GetXaxis()->SetTitle("t");

         h_t_true10 = new TH1D("h_t_true10","h_t_true 10mrad",100, 0.,2.);
        h_t_true10->SetDirectory(dir_main); h_t_jpsi10->GetXaxis()->SetTitle("t");
        h_t_true5 = new TH1D("h_t_true5","h_t_true 5mrad",100, 0.,2.);
        h_t_true5->SetDirectory(dir_main); h_t_jpsi5->GetXaxis()->SetTitle("t");
        h_t_true2 = new TH1D("h_t_true2","h_t_true 2mrad",100, 0.,2.);
        h_t_true2->SetDirectory(dir_main); h_t_jpsi2->GetXaxis()->SetTitle("t");

        h2_E_mytheta = new TH2I("h2_E_mytheta"," E vs theta ",100, 0.0,400.,100, -0., 30.);
        h2_E_mytheta->SetDirectory(dir_main); h2_E_mytheta->GetXaxis()->SetTitle("E(GeV)");h2_E_mytheta->GetYaxis()->SetTitle("#theta");


        h2_theta_t_true = new TH2I("h2_theta_t_true"," theta vs t",100, 0.00,30.,100, 0., 2.);
        h2_theta_t_true->SetDirectory(dir_main); h2_theta_t_true->GetXaxis()->SetTitle("#theta (mrad)");h2_theta_t_true->GetYaxis()->SetTitle("t (GeV)");



        h_xL = new TH1D("h_xL"," xL",100, 0.0, 1.);
        h_xL->SetDirectory(dir_main); h_xL->GetXaxis()->SetTitle("xL");
        h2_xL_t = new TH2I("h2_xL_t"," xL vs t ",100, 0.0,1.,100, 0., 2.);
        h2_xL_t->SetDirectory(dir_main); h2_xL_t->GetXaxis()->SetTitle("xL");h2_xL_t->GetYaxis()->SetTitle("t");

        h2_xL_ptn = new TH2I("h2_xL_ptn"," xL vs pt ",100, 0.0,1.,100, 0., 2.);
        h2_xL_ptn->SetDirectory(dir_main); h2_xL_ptn->GetXaxis()->SetTitle("xL");h2_xL_ptn->GetYaxis()->SetTitle("t");

        h2_xL = new TH2I("h2_xL"," theta vs xL",100, 0.00,30.,100, 0., 1.);
        h2_xL->SetDirectory(dir_main); h2_xL->GetXaxis()->SetTitle("theta");h2_xL->GetYaxis()->SetTitle("xL");

        h2_mytheta_myptn = new TH2I("h2_mytheta_myptn"," theta vs pt_n",100, 0.00,30.,100, 0., 2.);
        h2_mytheta_myptn->SetDirectory(dir_main); h2_mytheta_myptn->GetXaxis()->SetTitle("theta");

        h2_mytheta_myptn2 = new TH2I("h2_mytheta_myptn2"," theta vs pt_n^{2}",100, 0.00,30.,100, 0., 4.);
        h2_mytheta_myptn2->SetDirectory(dir_main); h2_mytheta_myptn2->GetXaxis()->SetTitle("theta");

        h_pt_d = new TH1D("h_pt_d"," Pt D' no cut ",100, 0.,2.);
        h_pt_d->SetDirectory(dir_main); h_pt_d->GetXaxis()->SetTitle("p_{T}^{D'}");

        h_pt_n = new TH1D("h_myptn"," Pt neutrons no cut ",100, 0.,2.);
        h_pt_n->SetDirectory(dir_main); h_pt_n->GetXaxis()->SetTitle("p_{T}^{n}");

        h_myptn10 = new TH1D("h_myptn10"," Pt neutrons  10mrad ",100, 0.,2.);
        h_myptn10->SetDirectory(dir_main); h_myptn10->GetXaxis()->SetTitle("p_{T}^{n}");
        h_myptn5 = new TH1D("h_myptn5"," Pt neutrons  5mrad ",100, 0.,2.);
        h_myptn5->SetDirectory(dir_main); h_myptn5->GetXaxis()->SetTitle("p_{T}^{n}");
        h_myptn2 = new TH1D("h_myptn2"," Pt neutrons  2mrad ",100, 0.,2.);
        h_myptn2->SetDirectory(dir_main); h_myptn2->GetXaxis()->SetTitle("p_{T}^{n}");

        h2_my_t_ptn= new TH2I("h2_my_t_ptn"," t vs pt_n ",100, 0.0,2.,100, 0., 2.);
        h2_my_t_ptn->SetDirectory(dir_main); h2_my_t_ptn->GetXaxis()->SetTitle("t");h2_my_t_ptn->GetYaxis()->SetTitle("Pt_n");
        h2_my_t_ptn2= new TH2I("h2_my_t_ptn2"," t vs pt_{n}^{2} ",100, 0.0,2.,100, 0., 4.);
        h2_my_t_ptn2->SetDirectory(dir_main); h2_my_t_ptn2->GetXaxis()->SetTitle("t");h2_my_t_ptn2->GetYaxis()->SetTitle("Pt_{n}^{2}");

        h2_t_true_jpsi= new TH2I("h2_t_true_jpsi","t_{true} vs t_{jpsi} ",100, 0.0,2.,100, 0., 2.);
        h2_t_true_jpsi->SetDirectory(dir_main); h2_t_true_jpsi->GetXaxis()->SetTitle("t_true");h2_t_true_jpsi->GetYaxis()->SetTitle("t_jpsi");
    }

    // ---- kinematic variables----
    TH2I *h2_XQ2_true_log, *h2_XQ2_true;
    TH1D *hX_true, *hQ2_true, *hY_true, *hW2_true, *hNu_true;
    TH1D *h_t_true,*h_t_true10,*h_t_true5,*h_t_true2;
    TH2I *h2_theta_t_true,*h2_theta_t_true10,*h2_theta_t_true5,*h2_theta_t_true2;

    //--- scattered e --------
    TH2I  *hXQ2_smear;
    TH1D *e_theta,*e_eta;
    // ------ J/psi ---------------

    TH1D *h_W2_jpsi, *h_Theta_jpsi;
    TH1D *h_pt_jpsi,*h_t_jpsi,*h_pt_jpsi10,*h_t_jpsi10,*h_pt_jpsi5,*h_t_jpsi5,*h_pt_jpsi2,*h_t_jpsi2;
    //-------------n, p, D   ------------------
    TH1D *h_myt_d,*h_myt_n;
    TH1D *h_myptn10,*h_myptn5,*h_myptn2;
    TH1D *h_pt_d,*h_pt_n;
    TH2I *h2_E_mytheta;
    TH1D *h_xL,*h_theta_n,*h_theta_p,*h_theta_d;
    TH2I *h2_theta_pn;
    TH2I *h2_xL_t,*h2_xL_ptn,*h2_xL;


    //----------- MY ------------------
    TH1D *h_mytheta,*h_myt;
    TH2I *h2_mytheta_myt, *h2_mytheta_t_true,*h2_theta_t_n,*h2_theta_t_d,*h2_theta_t_jpsi;
    TH2I *h2_mytheta_myptn, *h2_mytheta_myptn2;

    TH2I *h2_my_t_ptn,*h2_my_t_ptn2, *h2_t_true_jpsi;

    std::recursive_mutex lock;

    TTree * tree_rec_e;     // Tree to store electron related data
    TH2D *hpt_eth;

private:

    TDirectory* dir_main;   // Main TDirectory for Plugin histograms and data
};