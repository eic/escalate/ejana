#include "SampleAnalysis.h"

// Routine used to create our JEventProcessor
#include <JANA/JApplication.h>

extern "C" {
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new SampleAnalysis(app));
    }
}