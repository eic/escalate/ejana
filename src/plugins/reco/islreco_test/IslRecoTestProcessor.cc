
#include <JANA/JEventProcessor.h>
#include <JANA/Services/JGlobalRootLock.h>
#include <TH1D.h>
#include <TFile.h>
#include <Math/Vector4D.h>
#include <ejana/MainOutputRootFile.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <fmt/core.h>
#include <reco/islreco/CeEmcalCluster.h>
#include <io/g4e_reader/GeantCeEmcalData.h>
#include <io/g4e_reader/GeantPrimaryParticle.h>


class IslRecoTestProcessorProcessor: public JEventProcessor {
private:
    std::shared_ptr<ej::MainOutputRootFile> m_file;
    int m_verbose;                /// verbosity level. 0-none, 1-some, 2-all
    TDirectory* m_plugin_dir;     /// Virtual sub-folder inside root file used for this specific plugin

    /// Declare histograms here
    TH1D* h1d_pt;

public:
    explicit IslRecoTestProcessorProcessor() {
    }

    void Init() override {
        auto app = GetApplication();
        
        // Get main ROOT output file 
        m_file = app->GetService<ej::MainOutputRootFile>();

        // Set parameters that can be controlled from command line
        m_verbose = 1;
        app->SetDefaultParameter("islreco_test:verbose", m_verbose, "Verbosity level 0=none, 1=some, 2=all");

        if(m_verbose) {
            fmt::print("IslRecoTestProcessorProcessor::Init()\n  islreco_test:verbose={}\n", m_verbose);
        }

        // Setup histograms
        m_plugin_dir = m_file->mkdir("islreco_test"); // Create a subdir inside dest_file for these results
        m_plugin_dir->cd();
        h1d_pt = new TH1D("e_pt", "electron pt", 100,0,10);
    }

    void Process(const std::shared_ptr<const JEvent>& event) override {
        // This function is called every event
        if(m_verbose == 2) fmt::print("Begin of event {} \n", event->GetEventNumber());

        // Acquire any results you need for your analysis
        auto particles = event->Get<CeEmcalCluster>();

        // Go through particles
        for(auto& particle: particles) {
            fmt::print("islreco_test: id={:<15} E={:<15} x={:<15} y={:<15} chi2={:<15}\n", particle->id, particle->energy, particle->rel_x, particle->rel_y, particle->chi2);
        }

        auto gen_particles = event->Get<jleic::GeantPrimaryParticle>();
        double gen_sum_etot = 0;
        // Go through generated particles
        for(auto particle: gen_particles) {
            gen_sum_etot += particle->tot_e;
        }

        // Acquire any results you need for your analysis
        auto emcal_hits = event->Get<jleic::GeantCeEmcalData>();
        int hits_count = emcal_hits.size();

        double hit_sum_etot = 0;

        // Go through emcal hits
        for(size_t i=0; i< emcal_hits.size(); i++) {
            auto hit = emcal_hits[i];
            hit_sum_etot+=hit->etot_dep;
        }

        if(m_verbose >= 2) {
            fmt::print("=== cell_hit_count = {:<15}  hit_sum_etot = {:<15}  gen_sum_etot = {:<15}\n ===", hits_count, hit_sum_etot, gen_sum_etot);
        }
    }

    void Finish() override {
        fmt::print("IslRecoTestProcessorProcessor::Finish(). Cleanup\n");
    }
};

extern "C" {
    void InitPlugin(JApplication *app) {
        InitJANAPlugin(app);
        app->Add(new IslRecoTestProcessorProcessor());
    }
}
