
#include "RecoTrackFactory_Genfit.h"

#include <JANA/JEvent.h>
#include <reco/trk_fit/MagneticFieldService.h>

void RecoTrackFactory_Genfit::Init() {

    auto app = GetApplication();
    app->SetDefaultParameter("trk_fit_genfit:use_truth_for_estimate", use_truth_for_estimate);
    app->SetDefaultParameter("trk_fit_genfit:interactive", interactive);
    //magnetic_field = app->GetService<MagneticFieldService>();
}

void RecoTrackFactory_Genfit::ChangeRun(const std::shared_ptr<const JEvent> &event) {}

void RecoTrackFactory_Genfit::Process(const std::shared_ptr<const JEvent> &event) {

    EicGenfitInput input;
    input.clusters = event->Get<RecoTrackCand>();
    input.interactive = interactive;
    input.Bx = 0.0; //magnetic_field->Bx;
    input.By = 0.0; //magnetic_field->By;
    input.Bz = 30.0; //magnetic_field->Bz;

    if (use_truth_for_estimate) {
        input.truths = event->Get<minimodel::McTrack>();
    }

    genfit_results = FitTracks(input);

    Set(genfit_results.tracks);
    SetMetadata(genfit_results.metadata);
}
