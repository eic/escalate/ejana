

# islreco

## Python

Look at [pyjano_example.py](pyjano_example.py) to see the code.

**pyjano** automatically setups everything and re/builds a plugin when the code is changed. 


## Manual build and run from command line:

1. Environment

```bash
export ISLRECO_PROFILE=#<path to profile>
```
2. Jana searches compiled plugins using JANA_PLUGIN_PATH environment variable or in the current directory. 
Make sure you are running ejana where resulting .so file is located or set JANA_PLUGIN_PATH

3. Run 
```bash
ejana 
-Pplugins=islreco,hepmc_reader,open_charm
-Popen_charm:verbose=0
-Popen_charm:e_beam_energy=18
-Popen_charm:ion_beam_energy=275
-Pjana:DEBUG_PLUGIN_LOADING=1
<source >.hepmc
```
