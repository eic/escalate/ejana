

#include <JANA/JApplication.h>
#include <JANA/JFactoryGenerator.h>

#include "ActsPrimaryVertexFitter.h"
#include "ActsVertexFinder.h"


extern "C" {
void InitPlugin(JApplication* app) {

    // This code is executed when the plugin is attached.
    // It should always call InitJANAPlugin(app) first, and then do one or more of:
    //   - Read configuration parameters
    //   - Register JFactoryGenerators
    //   - Register JEventProcessors
    //   - Register JEventSourceGenerators (or JEventSources directly)
    //   - Register JServices

    InitJANAPlugin(app);

    LOG << "Loading trk_fit_acts" << LOG_END;
    app->Add(new JFactoryGeneratorT<ActsPrimaryVertexFitter>);
    app->Add(new JFactoryGeneratorT<ActsVertexFinder>);
    // Add any additional components as needed
}
}

