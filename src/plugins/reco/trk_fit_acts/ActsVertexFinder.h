
#ifndef _RecoTrackFactory_Acts_h_
#define _RecoTrackFactory_Acts_h_

#include <JANA/JFactoryT.h>
#include <RecoModel/RecoTrack.h>

class ActsVertexFinder : public JFactoryT<RecoTrack> {

    std::string m_reco_track_input = "truths";
    // "truths", "genfit"

public:
    ActsVertexFinder() : JFactoryT<RecoTrack>(NAME_OF_THIS, "acts") {};
    void Init() override;
    void ChangeRun(const std::shared_ptr<const JEvent> &event) override;
    void Process(const std::shared_ptr<const JEvent> &event) override;

};

#endif // _RecoTrackFactory_Acts_h_
