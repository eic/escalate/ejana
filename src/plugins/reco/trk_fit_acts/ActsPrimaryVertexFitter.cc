
#include "ActsPrimaryVertexFitter.h"

#include "Acts/MagneticField/ConstantBField.hpp"
#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/Vertexing/HelicalTrackLinearizer.hpp"
#include "Acts/Vertexing/FullBilloirVertexFitter.hpp"

#include <JANA/JEvent.h>

void ActsPrimaryVertexFitter::Init() {
    auto app = GetApplication();
    
    /// Acquire any parameters
    app->SetDefaultParameter("trk_fit_acts:input", reco_track_input);
    // Set to 'truths' to get RecoTracks straight from McTracks instead of from Genfit

    /// Acquire any services
    // m_service = app->GetService<ServiceT>();
    
    /// Set any factory flags
    // SetFactoryFlag(JFactory_Flags_t::NOT_OBJECT_OWNER);
}

void ActsPrimaryVertexFitter::ChangeRun(const std::shared_ptr<const JEvent> &event) {
    /// This is automatically run before Process, when a new run number is seen
    /// Usually we update our calibration constants by asking a JService
    /// to give us the latest data for this run number
    
    // auto run_nr = event->GetRunNumber();
    // m_calibration = m_service->GetCalibrationsForRun(run_nr);
}

void ActsPrimaryVertexFitter::Process(const std::shared_ptr<const JEvent> &event) {


    using MagneticField     = Acts::ConstantBField;
    using Stepper           = Acts::EigenStepper<MagneticField>;
    using Propagator        = Acts::Propagator<Stepper>;
    using PropagatorOptions = Acts::PropagatorOptions<>;
    using TrackParameters   = Acts::BoundTrackParameters;
    using Linearizer        = Acts::HelicalTrackLinearizer<Propagator>;
    using VertexFitter      = Acts::FullBilloirVertexFitter<TrackParameters, Linearizer>;
    using VertexingOptions = Acts::VertexingOptions<TrackParameters>;


    Acts::GeometryContext geo_context; // TODO: Figure out what ACTS does to initialize these
    Acts::MagneticFieldContext field_context;
    auto logger = Acts::getDefaultLogger("ActsVertexFinder", Acts::Logging::Level::VERBOSE);

    // Setup the magnetic field
    MagneticField bField(b_field); // TODO: What is the relationship between bField and field_context ?!?

    // Setup the propagator with void navigator
    auto propagator = std::make_shared<Propagator>(Stepper(bField));
    PropagatorOptions propagatorOpts(geo_context, field_context, Acts::LoggerWrapper(*logger));

    // Setup the vertex fitter
    VertexFitter::Config vertexFitterCfg;
    VertexFitter vertexFitter(vertexFitterCfg);

    VertexFitter::State vertexFitterState(nullptr);

    // Setup the linearizer
    Linearizer::Config ltConfig(bField, propagator);

    //ltConfig.pOptions.debug = true;
    Linearizer linearizer(ltConfig);

    // This is a wild guess, see FittingAlgorithm.cpp
    auto surface = Acts::Surface::makeShared<Acts::PerigeeSurface>(Acts::Vector3D{0., 0., 1});

    // For now assume all tracks correspond to the same vertex
    // This means there will only end up being one track collection
    std::vector<const Acts::BoundTrackParameters *> track_collection;
    auto tracks = event->Get<RecoTrack>(reco_track_input);

    /// Define parameter for pt-dependent IP resolution
    /// of the form sigma_d/z(p_t[GeV]) = A*exp(-B*p_t[GeV]) + C
    double ipResA = 30_um;
    double ipResB = 0.3 / 1_GeV;
    double ipResC = 20_um;

    /// Angular resolution
    double angRes = 0.05;
    /// q/p relative resolution factor
    double qpRelRes = 0.001;

    for (auto * track : tracks) {

        // TODO: Figure out how to get ACTS to give up if it doesn't make progress after so many steps
//        if (track->x.Mag2() > 3) { // TODO: Decide on/parameterize this threshold for nearness to primary vertex
//            // Filter out tracks which don't belong to a primary vertex.
//            // This means that our vertex fit should converge to (0,0,0)
//            LOG << "Filtering out non-primary track!" << LOG_END;
//            continue;
//        }

        // Repackage RecoTrack as Acts::BoundVector
        Acts::ActsVectorD<4> position (0, 0, 0, 0);
        Acts::ActsVectorD<3> momentum (track->p.Px(), track->p.Py(), track->p.Pz());
        double charge = track->charge;

        Acts::BoundSymMatrix covariance;
        covariance.setZero();
        // covariance.diagonal() << 1., 1., 1., 1., 1., 1.;

        const double particlePt = Acts::VectorHelpers::perp(momentum);

        const double ipRes = ipResA * std::exp(-ipResB * particlePt) + ipResC;

        // except for IP resolution, following variances are rough guesses
        // Gaussian distribution for IP resolution
        //std::normal_distribution<double> gaussDist_IP(0., ipRes);
        // Gaussian distribution for angular resolution
        //std::normal_distribution<double> gaussDist_angular(0., angRes);
        // Gaussian distribution for q/p (momentum) resolution


        double rn_d0 = ipRes;
        double rn_z0 = ipRes;
        double rn_ph = angRes;
        double rn_th = angRes;
        double rn_qp = qpRelRes;

        covariance.diagonal() << rn_d0 * rn_d0, rn_z0 * rn_z0, rn_ph * rn_ph, rn_th * rn_th, rn_qp * rn_qp, 1.;

	    Acts::BoundTrackParameters * bp = new Acts::BoundTrackParameters(surface, geo_context, position, momentum, charge, covariance);

	    track_collection.emplace_back(bp);
    }

    // Hand our track collection over to ACTS
    std::vector<std::pair<std::vector<const RecoTrack*>, std::vector<const Acts::BoundTrackParameters *>>> track_collections;
    track_collections.push_back(std::make_pair(std::move(tracks), std::move(track_collection)));

    std::vector<RecoTrack*> output;
    auto metadata = GetMetadata();

    for (const auto& track_pair : track_collections) {

        Acts::Vertex<TrackParameters> fittedVertex;
        if (!doConstrainedFit) {
            auto &input = track_pair.second;
            if (input.size() < 2) {
                metadata.insufficient_data_count++;
                LOG << "ActsPrimaryVertexFitter: Insufficent number of recotracks!" << LOG_END;
                continue;
            }
            // Vertex fitter options
            VertexingOptions vfOptions(geo_context, field_context);

            auto fitRes = vertexFitter.fit(track_pair.second, linearizer, vfOptions, vertexFitterState);
            if (fitRes.ok()) {
                fittedVertex = *fitRes;

                // Repackage fittedVertex as vector of RecoTracks
                for (auto track : track_pair.first) {
                    RecoTrack* new_track = new RecoTrack(*track);
                    // TODO: Is ACTS refitting the tracks? If so, we aren't capturing the refit
                    new_track->x.SetXYZ(fittedVertex.position().x(), fittedVertex.position().y(), fittedVertex.position().z());
                    output.push_back(new_track);
                }

            } else {
                LOG << "Error in vertex fit: " << fitRes.error().message() << LOG_END;
            }
        }
        else {
            // Vertex constraint
            Acts::Vertex<TrackParameters> theConstraint;

            theConstraint.setCovariance(constraintCov);
            theConstraint.setPosition(constraintPos);

            // Vertex fitter options
            VertexingOptions vfOptionsConstr(geo_context, field_context, theConstraint);

            auto fitRes = vertexFitter.fit(track_pair.second, linearizer, vfOptionsConstr, vertexFitterState);

            if (fitRes.ok()) {
                fittedVertex = *fitRes;

                for (auto track : track_pair.first) {
                    RecoTrack* new_track = new RecoTrack(*track);
                    // TODO: Is ACTS refitting the tracks? If so, we aren't capturing the refit
                    new_track->x.SetXYZ(fittedVertex.position().x(), fittedVertex.position().y(), fittedVertex.position().z());
                    output.push_back(new_track);
                }
                // TODO: Succeeded and misfit count are for vertices, not for tracks.
                metadata.succeeded_count++;
            } else {
                LOG << "Error in vertex fit: " << fitRes.error().message() << LOG_END;
                metadata.misfit_count++;
            }
        }
    }

    Set(output);
    SetMetadata(metadata);
}
