
Presentation
============

Can the user access the geometry, the kinematic data, and the reconstructed data together? 

Yes. All intermediate results of type T can be accessed via `event->Get<T>()`, which decouples the algorithm that produced
the result from whatever needs it. Caveat: The dependency graph must be acyclic. 


Is there a working tracking algorithm?

Yes. Genfit. Includes a parameter for toggling on and off the interactive event display. 
Caveats: Does not use RAVE yet. Fitter doesn't want to converge for tight helices. 


Can one switch between different tracking algorithms?

Yes. Not only can we swap out an algorithm completely by using plugins (as per example 1),
but we can also swap out the exact algorithm used in a single place (as per example 2).
We can do this at runtime.

Can one use multiple tracking algorithms at the same time?

Yes. See example 3.





 

Access to the geometry / segmentation and the kinematics : 
can the user access to the geometry and kinematic quantities in parallel with the reconstructed quantities? 

Can one user load the event i-th in reconstruction and access the event i-th in the kinematics? -> 
This doubts overlap with the "Simulation Needs" of the gaseous detector.​


Tracking algorithms : is there already one working algorithm in both softwares? ​

Can one switch from a general common tracking algorithm to a gaseous standalone one or to a Silicon standalone one? ​

Can several track finding algorithms be considered? ( Kalman filter, Cellular automaton, etc.).​

Comparing track finding


1. trk_eff cartoon which obtains geometry, reco tracks, and mc tracks, no tags. Can swap via plugins
2. trk_eff cartoon which obtains reco tracks depending on config parameter
3. trk_eff cartoon which obtains reco tracks three different ways
    
    How does information flow through ejana?
    - Domain model is expressed in JObjects. McFluxHit, McTrack, RecoHit, RecoTrackCand, etc.
    - JFactories can produce a JObject on demand, 
    - JEvent
    
    
    What already exists?
    
    
```c++
class TrackingEfficiencyProcessor : public JEventProcessor {
private:
    std::shared_ptr<JGlobalRootLock> m_lock;
    TH1D* h1d_pt_reco;
    TH1D* h1d_pt_mc;
    TH1D* h1d_pt_diff;

public:
    void Init() override {
        m_lock = GetApplication()->GetService<JGlobalRootLock>();

        h1d_pt_reco = new TH1D("pt_reco", "reco pt", 100,0,10);
        h1d_pt_mc = new TH1D("pt_mc", "reco mc", 100,0,10);
        h1d_pt_diff = new TH1D("pt_diff", "diff pt", 100,0,10);
    }

    void Process(const std::shared_ptr<const JEvent>& event) override {

        auto reco_tracks = event->Get<RecoTrack>();
        auto mc_tracks = event->Get<McTrack>();

        m_lock->acquire_write_lock();

        for (auto reco_track : reco_tracks) {
            h1d_pt_reco->Fill(reco_track->p.Pt());
        }
        // Fill other histograms
        m_lock->release_lock();
    }
```

```c++
class TrackingEfficiencyProcessor : public JEventProcessor {
private:
    std::string m_tracking_alg = "genfit";
    std::shared_ptr<JGlobalRootLock> m_lock;
    TH1D* h1d_pt_reco;

public:
    void Init() override {
        auto app = GetApplication();
        m_lock = app->GetService<JGlobalRootLock>();
        app->SetDefaultParameter("trk_eff:tracking_alg", m_tracking_alg);
        h1d_pt_reco = new TH1D("pt_reco", "reco pt", 100,0,10);

    }

    void Process(const std::shared_ptr<const JEvent>& event) override {

        auto reco_tracks = event->Get<RecoTrack>(m_tracking_alg);
        auto mc_tracks = event->Get<McTrack>();

        m_lock->acquire_write_lock();

        for (auto reco_track : reco_tracks) {
            h1d_pt_reco->Fill(reco_track->p.Pt());
        }

        m_lock->release_lock();
    }
```

```c++
class TrackingEfficiencyProcessor : public JEventProcessor {
private:
    std::shared_ptr<JGlobalRootLock> m_lock;
    TH1D* h1d_pt_reco;

public:
    void Init() override {
        m_lock = GetApplication()->GetService<JGlobalRootLock>();
        h1d_pt_reco = new TH1D("pt_reco", "reco pt", 100,0,10);
    }

    void Process(const std::shared_ptr<const JEvent>& event) override {

        auto genfit_tracks = event->Get<RecoTrack>("genfit");
        auto acts_tracks = event->Get<RecoTrack>("acts");
        auto mc_tracks = event->Get<McTrack>();

        m_lock->acquire_write_lock();

        for (auto reco_track : reco_tracks) {
            h1d_pt_reco->Fill(reco_track->p.Pt());
        }

        m_lock->release_lock();
    }
```

```
ejana -Pplugins=g4e_reader,trk_fit,trk_eff,trk_fit_acts g4e_output.root
```