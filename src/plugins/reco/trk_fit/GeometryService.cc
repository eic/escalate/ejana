//
// Created by Nathan Brei on 3/5/20.
//

#include <JANA/Services/JParameterManager.h>
#include "GeometryService.h"

void GeometryService::acquire_services(JServiceLocator *sl) {

    auto params = sl->get<JParameterManager>();
    params->SetDefaultParameter("trk_fit:geometry_root_filename", geometry_root_filename, "Full path to ROOT geometry file");
}
