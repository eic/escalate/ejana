//
// Created by Nathan Brei on 3/5/20.
//

#ifndef EJANA_GEOMETRYSERVICE_H
#define EJANA_GEOMETRYSERVICE_H


#include <JANA/Services/JServiceLocator.h>


class GeometryService : public JService {
public:

    ~GeometryService() override = default;
    void acquire_services(JServiceLocator* sl) override;


    std::string geometry_root_filename;
    double default_y_res = 0.1;
    double default_t_res = 0.1;
    std::map<std::string, std::pair<double, double>> resolutions_by_volume_name;

};


#endif //EJANA_GEOMETRYSERVICE_H
