
option(WITH_EIC_SMEAR "Add eic-smear plugin" OFF)

if(WITH_EIC_SMEAR)
    add_subdirectory(eic_smear)
endif()


#add_subdirectory(trk_fit)
add_subdirectory(islreco)
add_subdirectory(islreco_test)
# Acts packet
find_package(Acts COMPONENTS Core PluginIdentification PluginTGeo)
find_package(Boost 1.53.0 QUIET COMPONENTS filesystem program_options)

if(NOT Acts_FOUND)
    message(STATUS "(!) No ACTS package found. Building ejana without trk_fit_acts && trk_fit_genfit plugin")
endif()

if(NOT Boost_FOUND)
    message(STATUS "(!) No Boost package found. Building ejana without trk_fit_acts && trk_fit_genfit plugin")
endif()

if(Acts_FOUND AND Boost_FOUND)
#    add_subdirectory(trk_fit_acts)
#    add_subdirectory(trk_fit_genfit)
endif()

