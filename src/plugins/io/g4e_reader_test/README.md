

# g4e_reader_test

## Python

Look at [pyjano_example.py](pyjano_example.py) to see the code.

**pyjano** automatically setups everything and re/builds a plugin when the code is changed. 


## Manual build and run from command line:

1. Build:

```bash
cd g4e_reader_test
mkdir build && cd build
cmake ../
make
```

2. Jana searches compiled plugins using JANA_PLUGIN_PATH environment variable or in the current directory. 
Make sure you are running ejana where resulting .so file is located or set JANA_PLUGIN_PATH

3. Run 
```bash
ejana 
-Pplugins=g4e_reader,g4e_reader_test
-Pjana:DEBUG_PLUGIN_LOADING=1
<source>.root
```
