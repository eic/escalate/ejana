//
// Created by romanov on 1/20/21.
//

#ifndef EJANA_GEANTCEEMCALDATA_H
#define EJANA_GEANTCEEMCALDATA_H
namespace jleic {

    class GeantCeEmcalData: public JObject {
    public:
        std::string module_name;
        int id;
        int row;
        int col;
        int section_id;
        double etot_dep;
        int npe;
        double adc;
        double tdc;
        double crs_x;
        double crs_y;
        double crs_z;
    };
}

#endif //EJANA_GEANTCEEMCALDATA_H
